var http = require('http');
var fs = require('fs');
var path = require('path');
var index = fs.readFileSync('app/index.html');

http.createServer(function (req, res) {
    if(req.url=='/'){
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(index);
    }else{
        console.log(req.url);
        if(path.extname(req.url)=='.js'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'text/javascript'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.html'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'text/html'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.css'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'text/css'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.json'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'application/json'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.ttf'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'font/opentype'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.jpe' || path.extname(req.url)=='.jpeg' || path.extname(req.url)=='.jpg'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'image/jpeg'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.gif'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'image/gif'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.bmp'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'image/bmp'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.ico'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'image/x-icon'});
        res.end(archivo);
    }else if(path.extname(req.url)=='.png'){
        var archivo =fs.readFileSync(__dirname+req.url);
        res.writeHead(200,{'Content-Type': 'image/png'});
        res.end(archivo);
    }
    }


}).listen(process.env.PORT || 8080);
