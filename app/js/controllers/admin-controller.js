angular.module('Merralith')
    .controller("AdminController", ["Storage", "$rootScope", "$http", "$window", "$scope", function(Storage, $rootScope, $http, $window, $scope) {
        if ($rootScope.currentUser != undefined) {
            if ($rootScope.currentUser.tipo != "Admin") {
                $window.location.assign("#/home")
            }

        } else {
            $window.location.assign("#/home")
        }
        $rootScope.users = Storage.get("users");
        $rootScope.heroes = Storage.get("heroes");
        $http.get('app/data/enemigos.json').success(function(data) {
            $rootScope.monsters = data;
            $scope.selectCreatures = new Array($rootScope.monsters.length);
        });
        $http.get('app/data/armas.json').success(function(data) {
            $rootScope.objetos = data;
            $scope.selectItems = new Array($rootScope.objetos.length);
        });

        $scope.selectNews = new Array($rootScope.articles.length);
        $scope.selectHeroes = new Array($rootScope.heroes.length);
        $scope.selectUsers = new Array($rootScope.users.length);



        $scope.selectallUsers = false;
        $scope.selectallHeroes = false;
        $scope.selectallCreatures = false;
        $scope.selectallItems = false;
        $scope.selectallNews = false;

        $scope.changeUsers = function() {
            for (var i = 0; i < $scope.selectUsers.length; i++) {
                $scope.selectUsers[i] = $scope.selectallUsers;
            }
        }
        $scope.changeHeroes = function() {
            for (var i = 0; i < $scope.selectHeroes.length; i++) {
                $scope.selectHeroes[i] = $scope.selectallHeroes;
            }
        }
        $scope.changeCreatures = function() {
            for (var i = 0; i < $scope.selectCreatures.length; i++) {
                $scope.selectCreatures[i] = $scope.selectallCreatures;
            }
        }
        $scope.changeItems = function() {
            for (var i = 0; i < $scope.selectItems.length; i++) {
                $scope.selectItems[i] = $scope.selectallItems;
            }
        }
        $scope.changeNews = function() {
            for (var i = 0; i < $scope.selectNews.length; i++) {
                $scope.selectNews[i] = $scope.selectallNews;
            }
        }

        $scope.deleteSelectedUsers = function() {
            for (var i = 0; i < $scope.selectUsers.length; i++) {
                if($scope.selectUsers[i]){
                    console.log($scope.users[i]);
                }
            }

        }
        $scope.deleteSelectedHeroes = function() {
            for (var i = 0; i < $scope.selectHeroes.length; i++) {
                if($scope.selectHeroes[i]){
                    console.log($scope.heroes[i]);
                }
            }
        }
        $scope.deleteSelectedCreatures = function() {
            for (var i = 0; i < $scope.selectCreatures.length; i++) {
                if($scope.selectCreatures[i]){
                    console.log($rootScope.monsters[i]);
                }
            }
        }
        $scope.deleteSelectedItems = function() {
            for (var i = 0; i < $scope.selectItems.length; i++) {
                if($scope.selectItems[i]){
                    console.log($rootScope.objetos[i]);
                }
            }
        }
        $scope.deleteSelectedNews = function() {
            for (var i = 0; i < $scope.selectNews.length; i++) {
                if($scope.selectNews[i]){
                    console.log($rootScope.articles[i]);
                }
            }
        }

    }])
