angular.module('Merralith')
    .controller('ShopController', ['$rootScope', 'Storage', '$window', 'Session', '$http', function($rootScope, Storage, $window, Session, $http) {
        if (Storage.get('currentUser') == undefined) {
            $window.location.assign('/#/home')
        }
        $rootScope.currentProfile = Storage.get('currentProfile') || $rootScope.currentProfile;
        $rootScope.currentGold = Storage.get('currentProfile').gold;
        $http.get('app/data/armas.json').success(function(data) {
            $rootScope.objetos = data;


        });

        this.volver = function() {
            $window.location.assign("/#/gamemenu");
        }
        this.comprar = function(id) {

            var objeto = $rootScope.objetos[id - 1];
            var precio = objeto.precio;
            var currentInventory = Storage.get('currentInventory');
            if ($rootScope.currentProfile.gold >= precio) {

                for (var x = 0; x < currentInventory.length; x++) {
                    if (currentInventory[x].nombre == undefined) {
                        currentInventory[x] = objeto;
                        $rootScope.currentProfile.gold = $rootScope.currentProfile.gold - precio;
                        Storage.set('currentProfile', $rootScope.currentProfile);

                        var posHeroe = Session.getPositionHeroe(Storage.get('heroes'), Storage.get('currentProfile').name);
                        $rootScope.heroes = Storage.get('heroes');
                        $rootScope.heroes[posHeroe] = $rootScope.currentProfile;
                        Storage.set('heroes', $rootScope.heroes)
                        $rootScope.inventories[posHeroe] = currentInventory;
                        Storage.set('inventories', $rootScope.inventories);
                        Storage.set('currentInventory', currentInventory);
                        $rootScope.currentGold = Storage.get('currentProfile').gold;

                        alert("Has comprado " + objeto.nombre + " por " + precio + " oros");
                        $window.location.assign('/#/heroprofile');
                        break;
                    } else {
                        if (x == 5 && currentInventory[x].nombre != undefined) {
                            alert('No tienes más huecos en el inventario')
                        }
                    }
                }
            } else {
                alert("No tiene suficiente dinero para ese objeto");
            }
        }


    }]);
