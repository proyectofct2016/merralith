angular.module('Merralith')
    .controller('LoginController', ['$http', 'jwtHelper', 'Storage', 'Session', '$rootScope', '$window', '$scope', function($http, jwtHelper, Storage, Session, $rootScope, $window, $scope) {

        $rootScope.currentUser = Storage.get('currentUser');
        $rootScope.users = Storage.get('users') || [];
        Storage.set('users', $rootScope.users);
        $rootScope.heroes = Storage.get('heroes') || [{
            id: 1,
            name: "Heroe1",
            level: 3,
            atq: 5,
            def: 2,
            crit: 6,
            vit: 7
        }, {
            id: 2,
            name: "Enemy1",
            level: 1,
            atq: 5,
            def: 3,
            crit: 4,
            vit: 3
        }, {
            id: 3,
            name: "Heroelvl1",
            level: 1,
            atq: 4,
            def: 4,
            crit: 2,
            vit: 4
        }];
        Storage.set('heroes', $rootScope.heroes);
        if ($rootScope.currentUser) {
            this.username = $rootScope.currentUser.username || "";
            this.password = $rootScope.currentUser.pass || "";
            this.email = $rootScope.currentUser.email || "";
            this.account = $rootScope.currentUser.tipo || "";
            if ($rootScope.currentUser.tipo == "Admin") {

                this.isAdmin = true;
            }
        }


        /*this.login = function() {
            /*var logeado=Session.compruebaUser(this.username, this.password);

            $http.post('http://192.168.11.156:9090/login', {
                username: 'Antonio',
                password: '1234'
            }).then(function(response) {
                console.log(response);
                console.log(response.data);
                localStorageService.set('token', response.data);
                this.isConnected = Session.isValid();
                console.log(this.isConnected);
            }, function(error) {
                this.error = error;
                alert("No se ha podido iniciar Sesion");
            });*/

        this.login = function() {
            Session.compruebaUser(this.username, this.password);

            if ($rootScope.currentUser.idcampeon == "") {
                $window.location.assign("/#/createchamp");

            } else {
                $window.location.assign("/#/heroprofile");
            }
            if ($rootScope.currentUser.tipo == "Admin") {

                this.isAdmin = true;
            }
            this.username = $rootScope.currentUser.username || "";
            this.password = $rootScope.currentUser.pass || "";
            this.email = $rootScope.currentUser.email || "";
            this.account = $rootScope.currentUser.tipo || "";


        }

        this.cerrarSesion = function() {
            Session.cerrarSesion();
            if ($window.location.hash == "#/home") {
                $window.location.reload();
            } else {
                $window.location.assign("/#/home");
            }

        }


        /*};*/


    }]);
