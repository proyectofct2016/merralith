angular.module('Merralith')
    .controller('ProfileController', ['$rootScope', 'Storage', '$window', 'Session', '$scope','$http', function($rootScope, Storage, $window, Session, $scope,$http) {
        if (Storage.get('currentUser') == undefined) {
            $window.location.assign('/#/home')
        }

        $http.get('app/data/enemigos.json').success(function(data) {
            $rootScope.monsters = data;

        });

        $rootScope.users = Storage.get('users');
        $rootScope.heroes = Storage.get('heroes') || [{
            id: 1,
            name: "Heroe1",
            level: 3,
            atq: 5,
            def: 2,
            crit: 6,
            vit: 7
        }, {
            id: 2,
            name: "Enemy1",
            level: 1,
            atq: 5,
            def: 3,
            crit: 4,
            vit: 3
        }, {
            id: 3,
            name: "Heroelvl1",
            level: 1,
            atq: 4,
            def: 4,
            crit: 2,
            vit: 4
        }];
        Storage.set('heroes', $rootScope.heroes);
        $rootScope.currentUser = Storage.get('currentUser');
        $rootScope.inventories = Storage.get('inventories') || [
            [{}, {}, {}, {}, {}, {}, ],
            [{}, {}, {}, {}, {}, {}, ],
            [{}, {}, {}, {}, {}, {}, ]

        ];
        Storage.set('inventories', $rootScope.inventories);

        this.currentProfile = $rootScope.heroes[$rootScope.currentUser.idcampeon - 1];
        Storage.set('currentProfile', this.currentProfile);
        var posHeroe = Session.getPositionHeroe(Storage.get('heroes'), Storage.get('currentProfile').name);
        $rootScope.currentInventory = Storage.get('inventories')[posHeroe] || [{}, {}, {}, {}, {}, {}];
        Storage.set('currentInventory', $rootScope.currentInventory);
        this.calculaObjetos = function(stats) {
            for (var x = 0; x < $rootScope.currentInventory.length; x++) {
                if (Object.keys($rootScope.currentInventory[x]).length > 1) {
                    stats.atq = stats.atq + $rootScope.currentInventory[x].atq;
                    stats.def = stats.def + $rootScope.currentInventory[x].def;
                    stats.crit = stats.crit + $rootScope.currentInventory[x].crit;
                    stats.vit = stats.vit + $rootScope.currentInventory[x].vit;
                }

            }
            return stats;
        }
        Storage.set('currentProfile', this.currentProfile);
        $scope.stats = {
            atq: 0,
            def: 0,
            crit: 0,
            vit: 0

        }
        $scope.stats = this.calculaObjetos($scope.stats);
        Storage.set('stats', $scope.stats);


        this.volver = function() {
            $window.location.assign("/#/gamemenu");
        }




        this.vender = function(id) {
            var precio = $rootScope.currentInventory[id].precio;
            this.currentProfile.gold = this.currentProfile.gold + precio;
            $rootScope.currentInventory[id] = {};
            Storage.set('currentInventory', $rootScope.currentInventory);
            Storage.set('currentProfile', this.currentProfile);
            var invent = Storage.get('inventories');
            var posHeroe = Session.getPositionHeroe(Storage.get('heroes'), Storage.get('currentProfile').name);
            invent[posHeroe] = $rootScope.currentInventory;
            Storage.set('inventories', invent);
            $rootScope.heroes[posHeroe] = Storage.get('currentProfile');
            Storage.set('heroes', $rootScope.heroes);
            $scope.stats = {
                atq: 0,
                def: 0,
                crit: 0,
                vit: 0

            }
            $scope.stats = this.calculaObjetos($scope.stats);
            Storage.set('stats', $scope.stats);
        }
    }])
