angular.module('Merralith')
    .controller('RegisterController', ['RegistroService', 'Storage', '$rootScope', '$window', '$scope', function(RegistroService, Storage, $rootScope, $window, $scope) {
        if (Storage.get('currentUser') != undefined) {
            $window.location.assign('/#/home')
        }

        $rootScope.currentUser = Storage.get('currentUser') || "";
        this.users = Storage.get('users') || [];
        this.username = "";
        this.fullname = "";
        this.email = "";
        this.pass = "";
        this.pass2 = "";
        this.validusername = true;



        this.register = function() {
            if (this.pass == this.pass2 && $scope.form.$valid && this.username != "" && this.pass != "") {
                for (var x = 0; x < this.users.length; x++) {


                    if (this.username == this.users[x].username) {
                        this.validusername = false;
                        break;
                    }
                }

                if (this.validusername) {
                    var user = RegistroService.guardar(this.username, this.fullname, this.email, this.pass);
                    this.users.push(user);
                    Storage.set('users', this.users);
                    alert("El usuario " + user.username + " ha sido creado");
                    $rootScope.currentUser = Storage.get('currentUser') || this.users[this.users.length];
                    $window.location.assign("/#/home")
                } else {
                    alert("El usuario ya existe");
                }
            } else {
                alert("Algún campo introducido no es válido");
            };

        }
    }]);
