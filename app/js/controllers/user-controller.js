angular.module('Merralith')
    .controller('UserController', ['jwtHelper', 'Storage', 'Session', '$rootScope', '$window', function(jwtHelper, Storage, Session, $rootScope, $window) {
        if(Storage.get('currentUser')==undefined){
            $window.location.assign('/#/home')
        }
        $rootScope.currentUser = Storage.get('currentUser');

        this.volver = function() {
            $window.location.assign('/#/home');
        }

        this.saveUser = function() {
            Storage.set('currentUser', $rootScope.currentUser);
            alert('Sus Cambios han sido guardados');
        }
    }]);
