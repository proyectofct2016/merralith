angular.module('Merralith')
    .controller('ChampController', ['$rootScope', 'Storage', '$window', 'Session', function($rootScope, Storage, $window, Session) {
        if (Storage.get('currentUser') == undefined) {
            $window.location.assign('/#/home')
        }
        $rootScope.heroes = Storage.get('heroes');
        this.heroname = "";
        this.stats = [1, 1, 1, 1, 14]
        this.heroatq = 1;
        this.herodef = 1;
        this.herocrit = 1;
        this.herovit = 1;
        this.totalpoints = 14;
        this.gold = 100;
        this.exp = 0;


        this.sumaStat = function(stat) {
            if (this.stats[4] == 0) {

            } else {
                this.stats[stat] = this.stats[stat] + 1;
                this.stats[4] = this.stats[4] - 1;
            }



        }
        this.restaStat = function(stat) {
            if (this.stats[stat] <= 1) {
                return false;
            } else {
                this.stats[stat] = this.stats[stat] - 1;
                this.stats[4] = this.stats[4] + 1;
            }
        }

        this.registraCampeon = function() {
            if (this.stats[4] != 0) {
                alert("Aún tienes puntos sin gastar");
            } else {

                var id = $rootScope.heroes.length + 1;
                var heroname = this.heroname;
                var heroatq = this.stats[0];
                var herodef = this.stats[1];
                var herocrit = this.stats[2];
                var herovit = this.stats[3];
                var heroe = {
                    'id': id,
                    'name': heroname,
                    'level': 1,
                    'atq': heroatq,
                    'def': herodef,
                    'crit': herocrit,
                    'vit': herovit,
                    'gold': this.gold,
                    'exp': this.exp
                }
                var usuarios = Storage.get("users");
                var posHeroe = Session.getPositionUser(usuarios, $rootScope.currentUser.username);
                usuarios[posHeroe].idcampeon = $rootScope.heroes.length;
                $rootScope.heroes.push(heroe);
                Storage.set("heroes", $rootScope.heroes);
                $rootScope.currentUser.idcampeon = id;
                Storage.set("currentUser", $rootScope.currentUser);
                $window.location.assign("/#/heroprofile")
            }
        }

    }]);
