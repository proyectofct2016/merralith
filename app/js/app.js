angular.module('Merralith', ['ui.router', 'restangular', 'angular-jwt', 'LocalStorageModule'])
    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                views: {
                    'main': {
                        templateUrl: 'app/html/home.html',
                        controller: 'HomeController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('register', {
                url: '/register',
                views: {
                    'main': {
                        templateUrl: 'app/html/register.html',
                        controller: 'RegisterController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            })
            /*.state('login', {
                url: '/login',
                views: {
                    'main': {
                        templateUrl: 'html/home.html',
                        controller: 'HomeController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
        })*/
            .state('gamemenu', {
                url: '/gamemenu',
                views: {
                    'main': {
                        templateUrl: 'app/html/gamemenu.html',
                        controller: 'GameController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('createchamp', {
                url: '/createchamp',
                views: {
                    'main': {
                        templateUrl: 'app/html/createchamp.html',
                        controller: 'ChampController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('game', {
                url: '/play',
                views: {
                    'main': {
                        templateUrl: 'app/html/game.html',
                        controller: 'GameController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('profile', {
                url: '/heroprofile',
                views: {
                    'main': {
                        templateUrl: 'app/html/heroprofile.html',
                        controller: 'ProfileController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('shop', {
                url: '/shop',
                views: {
                    'main': {
                        templateUrl: 'app/html/shop.html',
                        controller: 'ShopController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('modUser', {
                url: '/modUser',
                views: {
                    'main': {
                        templateUrl: 'app/html/modUser.html',
                        controller: 'UserController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('chooseEnemy', {
                url: '/chooseEnemy',
                views: {
                    'main': {
                        templateUrl: 'app/html/chooseEnemy.html',
                        controller: 'Heroe2Controller',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('about', {
                url: '/about',
                views: {
                    'main': {
                        templateUrl: 'app/html/about.html',
                        controller: 'ContactController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            }).state('adminPanel', {
                url: '/adminPanel',
                views: {
                    'main': {
                        templateUrl: 'app/html/adminPanel.html',
                        controller: 'AdminController',
                        controllerAs: 'vm'
                    },
                    'sidebar': {
                        templateUrl: 'app/html/login_sidebar.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            });

        $urlRouterProvider.otherwise('/home');
    })
    .config(function(RestangularProvider) {
        //RestangularProvider.setDefaultRequestParams();
        RestangularProvider.setBaseUrl('http://192.168.11.156:9090');
    })
    .run(function() {});
