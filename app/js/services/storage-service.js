angular.module('Merralith')
    .service('Storage', ['localStorageService', function(localStorageService) {
        this.keys = localStorageService.keys;

        if (localStorageService.isSupported) {
            this.set = localStorageService.set;
            this.get = localStorageService.get;
            this.remove = localStorageService.remove;
            this.clearAll = localStorageService.clearAll;
        } else {

            this.set = localStorageService.cookie.set;
            this.get = localStorageService.cookie.get;
            this.remove = localStorageService.cookie.remove;
            this.clearAll = localStorageService.cookie.clearAll;
        }
    }])
