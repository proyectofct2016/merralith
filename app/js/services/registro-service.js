angular.module('Merralith')
    .service('RegistroService', ['$http', 'Storage', 'Session', function($http, Storage, Session) {

        this.guardar = function(username, fullname, email, pass, cuenta) {
            var usuario = new Object();
            usuario.username = username;
            usuario.fullname = fullname;
            usuario.email = email;
            usuario.pass = pass;
            usuario.tipo = "user";
            usuario.idcampeon = "";

            return usuario;
        }

    }])
