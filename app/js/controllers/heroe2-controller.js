angular.module('Merralith')
    .controller('Heroe2Controller', ["$scope", "$rootScope", "Storage", '$window', "Session", '$http', function($scope, $rootScope, Storage, $window, Session, $http) {
        if (Storage.get('currentUser') == undefined) {
            $window.location.assign('/#/home')
        }
        $http.get('app/data/enemigos.json').success(function(data) {
            $rootScope.monsters = data;

        });
        $rootScope.currentUser = Storage.get('currentUser');
        $rootScope.currentProfile = Storage.get('currentProfile') || $rootScope.currentProfile;
        $rootScope.heroes = Storage.get('heroes');
        $scope.currentHeroe = $rootScope.currentUser.idcampeon - 1;
        $rootScope.heroe1 = $rootScope.heroes[$scope.currentHeroe];
        $rootScope.heroe2 = {};


        $scope.elegir = function(num) {

            $scope.currentHeroe = $rootScope.currentUser.idcampeon - 1;
            $rootScope.heroe1 = $rootScope.heroes[$scope.currentHeroe];
            $rootScope.heroe2 = $rootScope.heroes[num];
            $rootScope.heroe2.image = "/app/images/personajes/personaje2.jpg"
            $rootScope.heroe1.image = "/app/images/personajes/personaje1.jpg";
            $window.location.assign("/#/play");


        }
        $scope.elegirmonster = function(num) {

            $scope.currentHeroe = $rootScope.currentUser.idcampeon - 1;
            $rootScope.heroe1 = $rootScope.heroes[$scope.currentHeroe];
            $rootScope.heroe2 = $rootScope.monsters[num];
            $rootScope.heroe1.image = "/app/images/personajes/personaje1.jpg";
            $window.location.assign("/#/play");


        }

        $scope.showEnemy = function() {
            $window.location.assign("/#/chooseEnemy");
        }

    }]);
