var gulp = require('gulp');
var browserSync = require('browser-sync');


gulp.task('server', function(){
    browserSync.init({
        server: {
            baseDir: ['app', 'node_modules']
        }
    });

    gulp.watch([
        'app/**/*'
    ], browserSync.reload);
});


gulp.task('default', function(){
    console.log('hello!');
});
