angular.module('Merralith')
    .controller('GameController', ["CombatService", "$scope", "$interval", "$rootScope", "Storage", '$window', 'Session', function(CombatService, $scope, $interval, $rootScope, Storage, $window, Session) {
        $scope.fighting=false;
        if (Storage.get('currentUser') == undefined) {
            $window.location.assign('/#/home')
        }
        if ($window.location.hash == "#/play") {

            if ($rootScope.heroe2 == undefined) {
                $window.location.assign("/#/chooseEnemy");
            }
        }

        $scope.herohp = {
            backgroundColor: 'red',
            width: '100%'
        }
        $scope.enemyhp = {
            backgroundColor: 'red',
            width: '100%'
        }
        $scope.damage1=0;
        $scope.damage2=0;
        $rootScope.currentUser = Storage.get('currentUser');
        $rootScope.currentProfile = Storage.get('currentProfile') || $rootScope.currentProfile;
        $rootScope.heroes = Storage.get('heroes');


        Storage.set('heroes', $rootScope.heroes);

        if ($rootScope.currentUser.idcampeon == "") {
            $window.location.assign("/#/createchamp");

        } else {

            $scope.currentHeroe = $rootScope.currentUser.idcampeon - 1;

        }

        this.stats = Storage.get('stats');
        // Fin datos del Servidor

        $scope.combat = function() {
            $scope.fighting=true;
            $scope.herohp = {
                backgroundColor: 'red',
                width: '100%'
            }
            $scope.enemyhp = {
                backgroundColor: 'red',
                width: '100%'
            }
            var weapon = Session.getWeapon($rootScope.heroe1);
            $scope.herovida1 = ($rootScope.heroe1.vit + weapon.vit) * 10 * $rootScope.heroe1.level;
            $scope.herovida2 = $rootScope.heroe2.vit * 10 * $rootScope.heroe2.level;
            $scope.oro = "";
            $scope.exp = "";
            $scope.ganador = "";
            $scope.winner = CombatService.combat($rootScope.heroe1, $rootScope.heroe2, $scope);

            var x = 1;

            var killInterval = $interval(function() {
                    var combate = $scope.winner[1];



                    $scope.damage1 = combate[x].damturno1;
                    $scope.damage2 = combate[x].damturno2;
                    $scope.herovida1 = combate[x].hero1vida;
                    $scope.herovida2 = combate[x].hero2vida;
                    $scope.turno = combate[x].turno;
                    $scope.herohp.width = combate[x].barravida1 + '%';
                    $scope.enemyhp.width = combate[x].barravida2 + '%';
                    x++;
                    if (x == $scope.winner[1].length) {

                        $scope.oro = $scope.winner[0].goldwin;
                        $scope.exp = $scope.winner[0].expwin;
                        $scope.ganador = $scope.winner[0].name;
                        var jugador = Storage.get('currentProfile');
                        if ($scope.winner[0].name == $rootScope.heroe1.name) {

                            if ((jugador.exp) + $scope.exp >= (100 * jugador.level)) {
                                var expsobra = jugador.exp + $scope.exp - (100 * jugador.level);

                                jugador.level = jugador.level + 1;

                                jugador.exp = expsobra;

                                alert('Has subido al nivel:' + jugador.level + '!!');
                            } else {
                                jugador.exp = $scope.exp + jugador.exp;

                            }
                            jugador.gold = jugador.gold + $scope.oro;
                        } else {

                            jugador.gold = jugador.gold + $scope.oro;

                        }
                        Storage.set('currentProfile', jugador);
                        $rootScope.heroes[$rootScope.currentProfile.id - 1] = Storage.get('currentProfile');
                        Storage.set('heroes', $rootScope.heroes);
                        $scope.fighting=false;
                        $interval.cancel(killInterval);
                    }

                },
                1000)


        };


        $scope.showEnemy = function() {
            $window.location.assign("/#/chooseEnemy");
        }
        $scope.volver = function() {
            $window.location.assign("/#/gamemenu");
        }
        $scope.showHero = function() {
            $window.location.assign("/#/heroprofile");
        }
        $scope.showShop = function() {
            $window.location.assign("/#/shop");
        }



    }]);
