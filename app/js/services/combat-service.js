angular.module('Merralith')
    .service('CombatService', ['$window', 'Session', function($window, Session) {



        this.combat = function(hero1, hero2, scope) {
            var deferred = $.Deferred();
            var datosCombate = [];
            var hero1vida = "";
            var hero2vida = "";
            var weapon = Session.getWeapon(hero1);
            scope.hero1vida = (hero1.vit + weapon.vit) * 10 * hero1.level;
            scope.hero2vida = hero2.vit * 10 * hero2.level;
            var turno = 0;


            do {
                var dam1 = defineAtq(hero1);
                var def1 = defineDef(hero1);
                var crit1 = defineCrit(hero1);
                var dam2 = defineAtq(hero2);
                var def2 = defineDef(hero2);
                var crit2 = defineCrit(hero2);
                turno++;
                dam1 = getCritico(dam1, crit1);
                dam2 = getCritico(dam2, crit2);

                var damturno1 = getDamage(dam1, def2);
                var damturno2 = getDamage(dam2, def1);
                if (damturno1 <= 0) {
                    damturno1 = 1;
                }
                if (damturno2 <= 0) {
                    damturno2 = 1;

                }

                hero2vida = scope.hero2vida - damturno1 ;
                hero1vida = scope.hero1vida - damturno2;

                datosCombate[turno] = mostrarCombate(turno, damturno1, damturno2, hero1vida, hero2vida, hero1, hero2, scope,Session);

            } while (scope.hero1vida >= 0 && scope.hero2vida >= 0)

            if (scope.hero1vida > scope.hero2vida) {
                hero1.turno = turno;
                hero1.goldwin = 50 + ((100 * hero2.level) / 2);
                hero1.expwin = 12.5 + ((25 * hero2.level) / 2);
                var winner = []
                winner.push(hero1);
                winner.push(datosCombate);
                return winner;

            } else {

                hero2.turno = turno;
                hero2.goldwin = 25;
                hero2.expwin = 0;
                var winner = []
                winner.push(hero2);
                winner.push(datosCombate);
                return winner;
            }
        }

        defineAtq = function(hero) {
            var weapon = Session.getWeapon(hero);
            return (hero.atq + weapon.atq) * hero.level;
        }
        defineDef = function(hero) {
            var weapon = Session.getWeapon(hero);
            return (hero.def + weapon.def) * hero.level;
        }
        defineCrit = function(hero) {
            var weapon = Session.getWeapon(hero);
            return ((hero.crit + weapon.crit) * hero.level) + (hero.crit + weapon.crit);
        }

    }])

getDamage = function(dam, def) {
    return dam - def;

}

getCritico = function(dam, crit) {
    var random = Math.floor(Math.random() * (100 - 1)) + 1;
    if (random <= crit) {
        dam = (dam * 1.5) + dam;
    }
    return dam;
}

mostrarCombate = function(turno, damturno1, damturno2, hero1vida, hero2vida, hero1, hero2, scope,Session) {

    var datosCombate = [];
    datosCombate.turno = turno;
    scope.hero1vida = hero1vida;
    scope.hero2vida = hero2vida;
    datosCombate.barravida1 = (scope.hero1vida * 100)/ ((hero1.vit+Session.getWeapon(hero1).vit) * 10*hero1.level);
    datosCombate.barravida2 = (scope.hero2vida * 100) / (hero2.vit * 10*hero2.level);
    datosCombate.damturno1 = damturno1;
    datosCombate.damturno2 = damturno2;
    datosCombate.hero1vida = hero1vida;
    datosCombate.hero2vida = hero2vida;

    return datosCombate;
}
