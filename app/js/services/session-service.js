angular.module('Merralith')
    .service('Session', ['Storage', 'jwtHelper', '$filter', '$rootScope',function(Storage, jwtHelper, $filter, $rootScope, Session) {


        $rootScope.users = Storage.get('users') || [{}];
        $rootScope.heroes = Storage.get('heroes');
        $rootScope.inventories = Storage.get('inventories');
        /*this.isValid = function() {
            var token = Storage.get('token');

            return token && !jwtHelper.isTokenExpired(token);

        };*/


        this.cerrarSesion = function() {
            this.guardarInfo();
            this.borrarInfo();
        }
        this.compruebaUser = function(user, pass) {
            var users = Storage.get('users');

            var loggeduser = $filter('getByUsername')(users, user);

            if (loggeduser) {
                if (loggeduser.pass == pass) {
                    Storage.set('currentUser', loggeduser);
                    $rootScope.currentUser = Storage.get('currentUser');
                    alert('Bienvenido ' + loggeduser.username);
                    return true;
                } else {
                    alert('La contraseña introducida es incorrecta');
                    return false;
                }
            } else {
                alert("El usuario no existe");
                return false;
            }
        }

        this.getPositionHeroe = function(heroes, heroe) {
            for (var i = 0; i < heroes.length; i++) {
                if (heroes[i].name == heroe) {

                    return i;
                }
            }
        }
        this.borrarInfo = function() {
            Storage.remove('currentProfile');
            Storage.remove('currentUser');
            Storage.remove('currentInventory');
            Storage.remove('stats');
        }

        this.guardarInfo = function() {
            var posHeroe = this.getPositionHeroe(Storage.get('heroes'), Storage.get('currentProfile').name);
            $rootScope.heroes[posHeroe] = Storage.get('currentProfile');
            Storage.set('heroes', $rootScope.heroes);
            $rootScope.inventories[posHeroe] = Storage.get('currentInventory');
            Storage.set('inventories', $rootScope.inventories);
            var posUser = this.getidAccount(Storage.get('users'), Storage.get('currentUser').username);
            $rootScope.users[posUser] = Storage.get('currentUser');
            Storage.set('users', $rootScope.users);
            var idPerfil = Storage.get('currentProfile').id;
            var usuarios = Storage.get('users');
            usuarios[posUser].idcampeon = idPerfil;
            Storage.set('users', usuarios);
        }

        this.getidAccount = function(users, user) {
            for (var i = 0; i < users.length; i++) {
                if (users[i].username == user) {
                    return i;
                }
            }
        }
        this.getPositionUser = function(heroes, heroe) {
            for (var i = 0; i < heroes.length; i++) {
                if (heroes[i].username == heroe) {

                    return i;
                }
            }
        }

        this.getWeapon = function(hero) {

            if (hero.name == Storage.get('currentProfile').name) {
                return Storage.get('stats');
            } else {
                return stats = {
                    atq: 0,
                    def: 0,
                    crit: 0,
                    vit: 0
                }
            }
        }

    }])



.filter('getByUsername', function() {
    return function(input, usuario) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (input[i].username == usuario) {
                return input[i];
            }
        }
        return null;
    }
});
